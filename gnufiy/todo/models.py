from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class DifficultyLevel(models.Model):
    name = models.CharField(max_length=15)
    level = models.IntegerField()
    
    def __unicode__(self):
        return self.name


class Item(models.Model):
    """
    Maintains Todo items
    """
    user = models.ForeignKey(User)
    name = models.CharField(max_length=60)
    description = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    priority = models.IntegerField(default=0)
    difficulty = models.ForeignKey(DifficultyLevel)
    done = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.name
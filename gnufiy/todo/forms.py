from django.forms import Form, ModelForm
from django import forms
from .models import Item

class TodoItemForm(ModelForm):
    
    class Meta:
        model = Item
        exclude = ("user", "done")
        
    def save(self, user, **kwargs):
        self.instance.user = user
        return super(TodoItemForm, self).save(**kwargs)
    
class SearchForm(Form):
    """
    Form to use to collect search parameters
    """
    sort_choices = (("name", "name"), 
                    ("status", "status"), 
                    ("priority", "priority"), 
                    ("diff", "diff"), 
                    ("date", "date")
                )
    done = forms.BooleanField(required=False)
    sort = forms.ChoiceField(choices=sort_choices,required=False)
    
    def filter(self, queryset):
        data = self.cleaned_data
        if data.get("done"):
            queryset = queryset.filter(done=False)
    
        if data.get("sort"):
            if data.get("sort") == "name":
                queryset = queryset.order_by(data.get("sort"))
            if data.get("sort") == "date":
                queryset = queryset.order_by("created")
            if data.get("sort") == "status":
                queryset = queryset.order_by("done")
            if data.get("sort") == "priority":
                queryset = queryset.order_by(data.get("sort"))
            if data.get("sort") == "diff":
                queryset = queryset.order_by("difficulty__level")
            
        return queryset
# Create your views here.
from django.shortcuts import render, redirect, get_object_or_404
from .models import Item
from .forms import TodoItemForm
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http.response import Http404
from django.core.exceptions import PermissionDenied
from .forms import SearchForm

def home(request):
    """
    Renders the home page
    """
    return render(request, "home.html", {})


from django.contrib.auth.views import logout as auth_logout
def logout(request):
    return auth_logout(request)

@login_required
def list_todo_items(request):
    """
    List all todo items for given user
    """
    items = Item.objects.filter(user = request.user)
    form = SearchForm(request.REQUEST)
    if form.is_valid():
        items = form.filter(items)
    
    return render(request, "todo/list.html", {"items": items})

@login_required
def add_item(request):
    """
    Handles the creation/updation of task item
    """
    form = TodoItemForm()
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save(request.user)
            return redirect(reverse("my_items_page"))
    return render(request, "todo/add.html", {"form": form})
    
    
@login_required
def delete_item(request, id):
    """
    Deletes the item
    """
    item = get_object_or_404(Item, id=id)
    if not item.user == request.user:
        raise PermissionDenied
    item.delete()
    return redirect(reverse("my_items_page"))


@login_required
def mark_item_done(request, id):
    """
    Marks the given item as completed
    """
    item = get_object_or_404(Item, id=id)
    if not item.user == request.user:
        raise PermissionDenied
    item.done = True
    item.save()
    return redirect(reverse("my_items_page"))
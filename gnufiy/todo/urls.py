from django.conf.urls import patterns, include, url

urlpatterns = patterns("todo",
                       
    url("list/", "views.list_todo_items", name="my_items_page"),
    url("add/", "views.add_item", name="add_items"),
    url("delete/(?P<id>\d)", "views.delete_item", name="delete_item"),
    url("mark/done/(?P<id>\d)", "views.mark_item_done", name="mark_item_done"),
    
)
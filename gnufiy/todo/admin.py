from django.contrib import admin
from .models import Item, DifficultyLevel
from django.contrib.auth.models import User

class ItemAdmin(admin.ModelAdmin):
    list_display = ["user", "name", "priority", "difficulty", "created", "done"]
    list_editable = ["done", "priority"]
    search_fields = ["name"]

class DifficultyLevelAdmin(admin.ModelAdmin):
    list_display = ["name", "level"]
    search_fields = ["name"]


admin.site.register(Item, ItemAdmin)
admin.site.register(DifficultyLevel, DifficultyLevelAdmin)